ABOUT
--------------------------------------------------------------------------------


REQUIREMENTS
--------------------------------------------------------------------------------
- Libraries module.


FEATURES
--------------------------------------------------------------------------------


INSTALLATION
--------------------------------------------------------------------------------


HOW TO USE?
--------------------------------------------------------------------------------
$zillow = zillow_load();
$params = array(
    'zpid' => 30728555,
    'rentzestimate' => FALSE,
);
$response = $zillow->call('GetZestimate', $params);
// Only fetch the zestimate amount.
$zestimate = $response->fetch('response|zestimate|amount');
