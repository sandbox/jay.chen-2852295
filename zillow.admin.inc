<?php

function zillow_admin_settings() {
    return drupal_get_form('zillow_admin_settings_form');
}

function zillow_admin_settings_form($form, &$form_state) {
    $form['zillow'] = array(
        '#tree' => FALSE,
    );
    $form['zillow']['zillow_zws_id'] = array(
        '#required' => TRUE,
        '#type' => 'textfield',
        '#title' => t('ZWS ID'),
        '#description' => '',
        '#default_value' => variable_get('zillow_zws_id', ''),
    );
    return system_settings_form($form);
}
